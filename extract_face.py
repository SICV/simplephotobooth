

from simplephotobooth import get_faces
import argparse
import numpy, cv2
from PIL import Image, ImageOps, ImageEnhance
import os

ap = argparse.ArgumentParser("")
ap.add_argument("image")
args = ap.parse_args()
face_cascade = cv2.CascadeClassifier(os.path.join("./models", 'haarcascade_frontalface_default.xml'))

im = Image.open(args.image).convert("RGB")
image_bgr = im
im_bgr = cv2.cvtColor(numpy.array(im), cv2.COLOR_RGB2BGR)

faces = get_faces(face_cascade, im_bgr)
print (len(faces), "faces")

def fit_size (im, box):
    bw, bh = box
    iw, ih = im.size
    w = bw
    h = int((ih/iw) * w)
    if h <= bh:
        return (w, h)
    h = bh
    w = int((iw/ih) * h)
    return (w, h)

def make_print_version(im):
    im = im.convert("L")
    rw, rh = fit_size(im, (384, 10000))
    im = im.resize((rw, rh), resample=Image.BICUBIC) # Image.BICUBIC

    im = ImageOps.autocontrast(im, 5)
    im = ImageEnhance.Contrast(im).enhance(2.0)
    # im = ImageOps.equalize(im)
    im = im.convert("1")
    return im

for i, face in enumerate(faces):
    x, y, width, height = face['x'], face['y'], face['width'], face['height']
    x1, y1, x2, y2 = x, y, x+width, y+height
    current_face_bgr = im_bgr[y1:y2, x1:x2]

    fname = os.path.join("face{0}.jpg".format(i))
    current_face_pil = Image.fromarray(cv2.cvtColor(current_face_bgr,cv2.COLOR_BGR2RGB))
    # current_face_pil = make_print_version(current_face_pil)
    current_face_pil.save(fname)
    fname = os.path.join("face{0}.print.jpg".format(i))
    make_print_version(current_face_pil).save(fname)
