from __future__ import print_function
from escpos.printer import Usb
from urllib2 import urlopen
from PIL import Image
import sys
from escpos.exceptions import USBNotFoundError
import usb.core

from time import sleep

# 0416:5011

# brendan's
USB1 = 0x0416
USB2 = 0x5011

# citizen
# USB1 = 0x2730
# USB2 = 0x2002

""" Seiko Epson Corp. Receipt Printer M129 Definitions (EPSON TM-T88IV) """

# def wget (f, path):
#     bytes = 0
#     with open(path, "wb") as fout:
#         while True:
#             data = f.read(102400)
#             if not data:
#                 break
#             bytes += len(data)
#             fout.write(data)
#     print ("wget: {0} bytes to {1}".format(bytes, path), file=sys.stderr)
#     return bytes

# def save_file(f):
#     i = f.info()
#     ct = i.get("Content-type")
#     print ("content-type", ct)
#     if ct.startswith("image/jpeg"):
#         ext = ".jpg"
#     else:
#         ext = ".png"
#     fpath = "tmp"+ext
#     if wget(f, fpath):
#         return fpath

def centered_text (text, width=32):
    lines = []
    for line in text.splitlines():
        text = line.strip()[:width]
        l = len(text)
        mw = (width-l)/2
        lines.append(" "*mw + text)
    return "\n".join(lines)

def print_photostrip(data, copies=1):
    try:
        p = Usb(USB1,USB2,0)
        # citizen
        # p = Usb(USB1,USB2,0,0x81,0x02)
        for copy in range(copies):
            print ("printing copy {0}".format(copy+1))
            for i in data['items']:
                if "text" in i:
                    p.text(centered_text(i['text'])+"\n")
                    # p.text(i['text']+"\n")
                    # p.image(path)
                    # p.image("00001Tris.400x.dither.png")
                elif "imagef" in i:
                    f = open(i['imagef'])
                    im = Image.open(f)
                    p.image(im)
                elif "url" in i:
                    # p.text(centered_text(i['url']))
                    p.text(i['url']+"\n")
            # if copy+1 < copies:
            #     p.text("--------------------------------")
            p.cut()
            if copy+1 == copies:
                # extra space at the end
                p.cut()
        return True
    except USBNotFoundError as e:
        print ("Exception", e)
        return False
    except usb.core.USBError as e:
        print ("Exception", e)
        return False
    except Exception as e:
        print ("Exception", e)
        return False

    #p.text("Hello World\n")
    # p.image("00001Tris.400x.jpg")
    #p.image("00001Tris.400x.dither.png")
    #p.text("\n")
    #p.qr(content="http://recognitionmachine.constantvzw.org/p/1234567890/")
    #p.text("\n")
    #p.image("qr.test.png")
    # p.barcode('1324354657687','EAN13',64,2,'','')
    #p.cut()


def print_image (path, rotate):
    try:
        p = Usb(USB1,USB2,0)
        if path.startswith("http"):
            f = urlopen(i['image'])
            im = Image.open(f)
        else:
            im = Image.open(path)
        if rotate:
            im = im.rotate(90)
        p.image(im)
        p.cut()
        return True
    except USBNotFoundError:
        return False



# if __name__ == "__main__":
#     import argparse
#     ap = argparse.ArgumentParser("")
#     ap.add_argument("--rotate", action="store_true", default=False)
#     ap.add_argument("path", nargs="+")
#     args = ap.parse_args()
#     for n in args.path:
#         print_image(n, rotate=args.rotate)