

    apt get install python-opencv
    pip install imutils picamera python-escpos



<https://github.com/adafruit/Python-Thermal-Printer/blob/master/Adafruit_Thermal.py>

Hardware setup
-------------------
Using the "BCM" pin ordering
Pins 1 + 2 are the top pins when viewing pi sd card up / usb ports down -- gpio pins on the top right of profile.

Wiring
=============

Shutdown Button
-----------------
Pins 14 & 16 (7th & 8th in case-side) (gnd + gpio23)

in boot/config.txt
dtoverlay=gpio-shutdown,gpio_pin=23



Flash on Pi
--------------
GPIO -Purple -- Pin7: GPIO4
GND   Gray --- Pin 6 : Ground
GND   White? -- not connected
5V -- Black -- Pin 2: 5V

Button
-----------
35 Yellow GPIO19
37 Green GPIO26
39 Blue GND
(button 2 is purple, gray, white ipv yellow green blue)
(last three on second row)

Console size
-----------------
    sudo dpkg-reconfigure console-setup


